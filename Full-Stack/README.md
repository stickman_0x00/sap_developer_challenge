# [Full-Stack](https://blogs.sap.com/2023/09/06/sap-developer-challenge-full-stack-sap-cap-sap-fiori-elements/)

# [Project Set-up and Database Modeling (Week 1)](https://groups.community.sap.com/t5/application-development-discussions/sap-developer-challenge-full-stack-project-set-up-and-database-modeling/m-p/284674#M4000)

# [Back-end Development – API Exposure (Week 2)](https://groups.community.sap.com/t5/application-development-discussions/sap-developer-challenge-full-stack-back-end-development-api-exposure-week-2/m-p/286252#M4102)

# [Front-end Development with SAP Fiori Elements (Week 3)](https://groups.community.sap.com/t5/application-development-discussions/sap-developer-challenge-full-stack-front-end-development-with-sap-fiori/m-p/288213?lightbox-message-images-288478=44821iC5088AF7A1880669#M4181)

# [Integration of backend and frontend (Week 4)](https://community.sap.com/t5/application-development-discussions/sap-developer-challenge-full-stack-integration-of-backend-and-frontend-week/m-p/290772)

# Resources

- [CAP: Select inside Action](https://answers.sap.com/questions/13165150/select-inside-action.html)
