sap.ui.define(["sap/m/MessageBox"], function (MessageBox) {
	"use strict";

	return {
		addQuestions: function () {
			if (!this.pDialog) {
				this.pDialog = this.loadFragment({
					name: "fullstackdevchallenge.ext.addQuestions.AddQuestions",
				});
			}

			this.pDialog.then(function (oDialog) {
				oDialog.open();
			});
		},

		onClose: function () {
			sap.ui.getCore().byId("dialogAddQuestion").close();
		},

		onSubmit: function () {
			var formBinding = sap.ui
				.getCore()
				.byId("_IDGenSimpleForm1")
				.getObjectBinding();

			formBinding.execute().then(
				function () {
					MessageBox.information(
						formBinding.getBoundContext().getObject().value
					);

					sap.ui.getCore().byId("dialogAddQuestion").close();

					this.getBindingContext().refresh();
				}.bind(this),
				function (oError) {
					MessageBox.error(oError.message);
				}
			);
		},
	};
});
