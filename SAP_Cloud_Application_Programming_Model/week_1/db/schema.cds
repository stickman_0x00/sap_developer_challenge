namespace golf;

using {managed} from '@sap/cds/common';

entity Rounds : managed {
    key ID    : UUID;
        title : String(111);
        holes : Composition of many Holes
                    on holes.round = $self;
}

entity Holes {
    key ID    : UUID;
        round : Association to Rounds;
        shots : Composition of many Shots
                    on shots.hole = $self;
        score : Integer;
}

entity Shots {
    key ID   : UUID;
        hole : Association to Holes;
}
