# [SAP Developer Challenge – SAP Cloud Application Programming Model](https://blogs.sap.com/2023/07/05/sap-developer-challenge-sap-cloud-application-programming-model/)

# [SAP Cloud Application Programming Model (Week 1)](https://groups.community.sap.com/t5/application-development/sap-developer-challenge-sap-cloud-application-programming-model/m-p/270028#M1809)

# [SAP Cloud Application Programming Model (Week 2) ](https://groups.community.sap.com/t5/application-development/sap-developer-challenge-sap-cloud-application-programming-model/td-p/271512)

# [ SAP Cloud Application Programming Model (Week 3) ](https://groups.community.sap.com/t5/application-development/sap-developer-challenge-sap-cloud-application-programming-model/m-p/273060)

# [ SAP Cloud Application Programming Model (Week 4) ](https://groups.community.sap.com/t5/application-development/sap-developer-challenge-sap-cloud-application-programming-model/td-p/274851)

# Resources

-   [Understanding and exploring managed associations in CAP](https://github.com/qmacro/managed-associations-in-cap/tree/main/guide)
