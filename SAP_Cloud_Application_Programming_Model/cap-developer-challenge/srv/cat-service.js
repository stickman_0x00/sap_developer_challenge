class CatalogService extends cds.ApplicationService {
	async init() {
		const { Holes, Players } = this.entities;
		this.on("CREATE", Holes, this.onCreateHoles);

		const remote = await cds.connect.to("RemoteService");
		this.on("*", Players, (req) => {
			console.log(">> delegating to remote service...");
			return remote.run(req.query);
		});

		return super.init();
	}

	onCreateHoles(req, next) {
		const { score, par } = req.data;

		req.data.result =
			score - par === -3
				? "albatross"
				: score - par === -2
				? "eagle"
				: score - par === -1
				? "birdie"
				: score - par === 0
				? "par"
				: score - par === +1
				? "bogey"
				: score - par === +2
				? "double bogey"
				: score - par === +3
				? "triple bogey"
				: score === 1
				? "hole in one"
				: "";

		return next();
	}
}
module.exports = CatalogService;
