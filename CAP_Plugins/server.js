const cds = require("@sap/cds");

cds.on("serving", (service) => {
	const apiPath = "/api-docs" + service.path;
	console.log(`[Open API] - serving ${service.name} at ${apiPath}`);
	addLinkToGraphQl(service);
});

function addLinkToGraphQl(service) {
	const provider = (entity) => {
		if (entity) return; // avoid link on entity level, looks too messy
		return { href: "graphql", name: "GraphQl", title: "Show in GraphQL" };
	};
	// Needs @sap/cds >= 4.4.0
	service.$linkProviders
		? service.$linkProviders.push(provider)
		: (service.$linkProviders = [provider]);
}

module.exports = cds.server;
