const cds = require("@sap/cds");
const { Books } = cds.entities("sap.capire.bookshop");

class CatalogService extends cds.ApplicationService {
	init() {
		// Reduce stock of ordered books if available stock suffices
		this.on("submitOrder", async (req) => {
			const { book, quantity } = req.data;

			const audit = await cds.connect.to("audit-log");
			await audit.log("DevChallenge", {
				description: "Submit Order",
				uuid: req.data.id,
				tenant: req.data.tenant,
				user: req.user.id,
				time: req.data.time,
			});

			let { stock } = await SELECT`stock`.from(Books, book);
			if (stock >= quantity) {
				await UPDATE(Books, book).with(`stock -=`, quantity);
				await this.emit("OrderedBook", { book, quantity, buyer: req.user.id });
				return { stock };
			} else
				return req.error(409, `${quantity} exceeds stock for book #${book}`);
		});

		// Add some discount for overstocked books
		this.after("READ", "ListOfBooks", async (entrys) => {
			for (const each of entrys) {
				if (each.stock > 111) {
					each.title += ` -- 11% discount!`;

					const alert = await cds.connect.to("notifications");
					alert.notify({
						NotificationTypeKey: "ListOfBooksRead",
						NotificationTypeVersion: "1",
						Priority: "NEUTRAL",
						Properties: [
							{
								Key: "ID",
								IsSensitive: false,
								Language: "en",
								Value: each.ID,
								Type: "String",
							},
							{
								Key: "user",
								IsSensitive: false,
								Language: "en",
								Value: cds.context.user.id,
								Type: "String",
							},
							{
								Key: "title",
								IsSensitive: false,
								Language: "en",
								Value: each.title,
								Type: "String",
							},
							{
								Key: "stock",
								IsSensitive: true,
								Language: "en",
								Value: each.stock,
								Type: "String",
							},
						],
						Recipients: [
							{ RecipientId: "supportuser1@mycompany.com" },
							{ RecipientId: "supportuser2@mycompany.com" },
						],
					});
				}
			}
		});

		return super.init();
	}
}

module.exports = { CatalogService };
