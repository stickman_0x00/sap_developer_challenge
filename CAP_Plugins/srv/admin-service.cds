using { sap.capire.bookshop as my } from '../db/schema';
service AdminService @(requires:'admin') {
  entity Books as projection on my.Books;
  entity Authors as projection on my.Authors;

  annotate AdminService.Books with @changelog : [title, author.name] {
    title    @changelog;
    author   @changelog: [autor.name];
    genre   @changelog: [genre.name];
    stock   @changelog;
    descr   @changelog;
  };
}
