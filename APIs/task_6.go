package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

const url_categories = "https://developer-challenge.cfapps.eu10.hana.ondemand.com/odata/v4/northbreeze/Categories"
const url_category_me = url_categories + "(%d)?$select=CategoryID,CategoryName"

type Category struct {
	CategoryID   int    `json:"CategoryID"`
	CategoryName string `json:"CategoryName"`
	Description  string `json:"Description"`
}

func task_6() (string, error) {
	c := Category{
		CategoryID:   communityIDNumber,
		CategoryName: communityID,
		Description:  "August Developer Challenge",
	}
	json_c, err := json.Marshal(c)
	if err != nil {
		return "", err
	}

	_, err = http.Post(url_categories, "application/json", bytes.NewReader(json_c))
	if err != nil {
		return "", err
	}

	response, err := http.Get(fmt.Sprintf(url_category_me, communityIDNumber))
	if err != nil {
		return "", err
	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return "", fmt.Errorf("fail reading body: %v", err)
	}
	defer response.Body.Close()

	return hash_value(url.PathEscape(string(body)))
}
