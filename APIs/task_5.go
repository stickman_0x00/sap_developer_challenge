package main

import (
	"fmt"
	"io"
	"net/http"
)

const url_data_time_API = "https://sandbox.api.sap.com/dateandtime"

func date_time_API(endpoint string) (string, error) {
	request, err := http.NewRequest("GET", url_data_time_API+endpoint, nil)
	if err != nil {
		return "", fmt.Errorf("fail creating request: %v", err)
	}

	request.Header.Add("apikey", api_key)

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return "", fmt.Errorf("fail request: %v", err)
	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return "", fmt.Errorf("fail reading body: %v", err)
	}
	defer response.Body.Close()

	return string(body), nil
}

func task_5() (string, error) {
	value, err := date_time_API("/getCountryDateFormat?country=DE")
	if err != nil {
		return "", err
	}

	return hash_value(value + ",x-abap-response-time")
}
