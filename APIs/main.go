package main

import (
	"fmt"
	"log"
	"os"
)

const communityID = "stickman_0x00"
const communityIDNumber = 148321
const url_hash = "https://developer-challenge.cfapps.eu10.hana.ondemand.com/v1/hash(value='%s')"

var api_key string

func init() {
	api_key_file, err := os.ReadFile(".api_key")
	if err != nil {
		panic(err)
	}

	api_key = string(api_key_file)
}

func main() {
	// // task 0
	// communityID_hash, err := hash_value("this-is-the-year-of-the-api")
	// if err != nil {
	// 	log.Fatalf("error hashing community id: %v\n", err)
	// }

	// fmt.Printf("CommunityID hash: %s\n", communityID_hash)

	// // task 1
	// entities_hash, err := hash_entities("copy paste")
	// if err != nil {
	// 	log.Fatalf("error hashing entities: %v\n", err)
	// }

	// fmt.Printf("Entities hash: %s\n", entities_hash)

	// // task 2
	// stock_hash, err := hash_stock_products_discontinued()
	// if err != nil {
	// 	log.Fatalf("error hashing count products: %v\n", err)
	// }

	// fmt.Printf("Hash stock: %s\n", stock_hash)

	// // task 3
	// selected_product_hash, err := hash_selected_product()
	// if err != nil {
	// 	log.Fatalf("error hashing count products: %v\n", err)
	// }

	// fmt.Printf("Hash selected product: %s\n", selected_product_hash)

	// // task 4
	// data_time_paths_hash, err := hash_date_time_paths()
	// if err != nil {
	// 	log.Fatalf("error hashing date time paths: %v\n", err)
	// }

	// fmt.Printf("Hash date time paths: %s\n", data_time_paths_hash)

	// // task 5
	// task_5_hash, err := task_5()
	// if err != nil {
	// 	log.Fatalf("task 5: %v\n", err)
	// }

	// fmt.Printf("Hash of task 5: %s\n", task_5_hash)

	// // task 6
	// task_6_hash, err := task_6()
	// if err != nil {
	// 	log.Fatalf("task 6: %v\n", err)
	// }

	// fmt.Printf("Hash of task 6: %s\n", task_6_hash)

	// // task 7
	// task_7_hash, err := task_7()
	// if err != nil {
	// 	log.Fatalf("task 7: %v\n", err)
	// }

	// fmt.Printf("Hash of task 7: %s\n", task_7_hash)

	// task 8
	task_8_hash, err := task_8()
	if err != nil {
		log.Fatalf("task 8: %v\n", err)
	}

	fmt.Printf("Hash of task 7: %s\n", task_8_hash)
}
