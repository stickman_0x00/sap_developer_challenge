package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

// First way
// https://developer-challenge.cfapps.eu10.hana.ondemand.com/odata/v4/northbreeze/Products?$apply=filter(Discontinued%20eq%20false)/aggregate(UnitsInStock%20with%20sum%20as%20TotalStock)

// Second way
const url_products_discontinued = "https://developer-challenge.cfapps.eu10.hana.ondemand.com/odata/v4/northbreeze/Products?$apply=filter(Discontinued%20eq%20false)"

type Products struct {
	OdataContext string `json:"@odata.context"`
	Value        []struct {
		ProductID          int     `json:"ProductID"`
		ProductName        string  `json:"ProductName"`
		QuantityPerUnit    string  `json:"QuantityPerUnit"`
		UnitPrice          float64 `json:"UnitPrice"`
		CategoryCategoryID int     `json:"Category_CategoryID"`
		SupplierSupplierID int     `json:"Supplier_SupplierID"`
		UnitsInStock       int     `json:"UnitsInStock"`
		UnitsOnOrder       int     `json:"UnitsOnOrder"`
		ReorderLevel       int     `json:"ReorderLevel"`
		Discontinued       bool    `json:"Discontinued"`
	} `json:"value"`
}

func hash_stock_products_discontinued() (string, error) {
	// get entities
	response, err := http.Get(url_products_discontinued)
	if err != nil {
		return "", fmt.Errorf("fail requesting products: %v", err)
	}

	var products Products
	err = json.NewDecoder(response.Body).Decode(&products)
	if err != nil {
		return "", fmt.Errorf("fail decoding products: %v", err)
	}

	total := 0
	for _, product := range products.Value {
		total += product.UnitsInStock
	}

	return hash_value(strconv.Itoa(total))
}
