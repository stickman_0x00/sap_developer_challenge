package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

const url_seclect_product = "https://developer-challenge.cfapps.eu10.hana.ondemand.com/odata/v4/northbreeze/selectProduct"

type Selected_product struct {
	OdataContext string `json:"@odata.context"`
	Value        string `json:"value"`
}

func hash_selected_product() (string, error) {
	// get entities
	data := []byte("{\"communityid\":\"" + communityID + "\"}")
	response, err := http.Post(url_seclect_product, "application/json", bytes.NewReader(data))
	if err != nil {
		return "", fmt.Errorf("fail requesting products: %v", err)
	}

	var selected_product Selected_product
	err = json.NewDecoder(response.Body).Decode(&selected_product)
	if err != nil {
		return "", fmt.Errorf("fail decoding selected product: %v", err)
	}

	return hash_value(url.PathEscape(selected_product.Value))
}
