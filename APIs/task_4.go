package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
)

type DateAndTimeAPI struct {
	Paths map[string]interface{} `json:"paths"`
}

func hash_date_time_paths() (string, error) {
	file, err := os.ReadFile("./resources/DateAndTime.json")
	if err != nil {
		return "", fmt.Errorf("error reading the file: %v", err)
	}

	var api DateAndTimeAPI
	json.NewDecoder(bytes.NewReader(file)).Decode(&api)

	values := ""
	for key := range api.Paths {
		values += key + ":"

	}
	values = values[:len(values)-1]

	return hash_value(values)
}
