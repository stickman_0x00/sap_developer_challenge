# [SAP Developer Challenge – APIs](https://blogs.sap.com/2023/08/01/sap-developer-challenge-apis/)

# [Task 0 - Learn to share your task results](https://groups.community.sap.com/t5/application-development/sap-developer-challenge-apis-task-0-learn-to-share-your-task/m-p/276058)

# [Task 1 - List the Northwind entity sets](https://groups.community.sap.com/t5/application-development/sap-developer-challenge-apis-task-1-list-the-northwind-entity/m-p/276626)

# [Task 2 - Calculate Northbreeze product stock](https://groups.community.sap.com/t5/application-development/sap-developer-challenge-apis-task-2-calculate-northbreeze/m-p/277325/thread-id/2555)

# [Task 3 - Have a Northbreeze product selected for you](https://groups.community.sap.com/t5/application-development/sap-developer-challenge-apis-task-3-have-a-northbreeze-product/m-p/277972#M2727)

# [Task 4 - Discover the Date and Time API Package](https://groups.community.sap.com/t5/application-development/sap-developer-challenge-apis-task-4-discover-the-date-and-time/m-p/278745#M2897)
