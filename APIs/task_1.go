package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const url_northwind = "https://services.odata.org/V4/Northwind/Northwind.svc/"

type NorthWind_metadata struct {
	Value []struct {
		Name string `json:"name"`
		Kind string `json:"kind"`
		URL  string `json:"url"`
	} `json:"value"`
}

func hash_entities(communityID string) (string, error) {
	// get entities
	response, err := http.Get(url_northwind)
	if err != nil {
		return "", fmt.Errorf("fail requesting metadata: %v", err)
	}

	var metadata NorthWind_metadata
	err = json.NewDecoder(response.Body).Decode(&metadata)
	if err != nil {
		return "", fmt.Errorf("fail decoding metadata: %v", err)
	}

	// concatenate entities
	values := ""
	for _, entity := range metadata.Value {
		values += entity.Name + ","
	}
	values = values[:len(values)-1]

	// hash entities
	return hash_value(values)
}
