package main

import (
	"fmt"
	"io"
	"net/http"
)

func hash_value(value string) (string, error) {
	url := fmt.Sprintf(url_hash, value)
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", fmt.Errorf("fail creating request: %v", err)
	}

	request.Header.Add("CommunityID", communityID)

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return "", fmt.Errorf("fail request: %v", err)
	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return "", fmt.Errorf("fail reading body: %v", err)
	}
	defer response.Body.Close()

	return string(body), nil
}
